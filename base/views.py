from django.shortcuts import render, redirect
from django.db.models import Q
from django.http import HttpResponse
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from .models import Room, Topic, Message, User
from .forms import RoomForm, UserForm, MyUserCreationForm


# Create your views here.

# rooms = [
#     {'id': 1, 'name': 'Lets learn python!'},
#     {'id': 2, 'name': 'Design with me'},
#     {'id': 3, 'name': 'Frontend Developers'},
# ]

def loginPage(request):
    page = 'login'
    if request.user.is_authenticated:
        return redirect('home')

    if request.method == 'POST':
        email = request.POST.get('email')
        password = request.POST.get('password')

        try:
            User.objects.get(username=email)
        except:
            messages.error(request, 'User does not exist.')

        user = authenticate(request, email=email, password=password)

        if user is not None:
            login(request, user)
            return redirect('home')
        else:
            messages.error(request, 'Username or password does not exist.')

    context = {'page': page}
    return render(request, 'base/login_register.html', context)


def registerPage(request):
    form = MyUserCreationForm()

    if request.method == 'POST':
        form = MyUserCreationForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.username = user.username
            user.save()
            login(request, user)
            return redirect('home')
        else:
            messages.error(request, 'An error occurred during registration.')

    return render(request, 'base/login_register.html', {'form': form})


def logoutUser(request):
    logout(request)
    return redirect('home')


def home(request):
    q = request.GET.get('q') if request.GET.get('q') is not None else ''
    rooms = Room.objects.filter(Q(topic__name__icontains=q) |
                                Q(name__icontains=q) |
                                Q(description__icontains=q))
    topic = Topic.objects.all()[:5]
    room_count = rooms.count()
    room_messages = Message.objects.filter(Q(room__topic__name__icontains=q))[:12]

    context = {'rooms': rooms, 'topics': topic, 'room_count': room_count, 'room_messages': room_messages}
    return HttpResponse(render(request, 'base/home.html', context))


def room(request, pk):
    get_room = Room.objects.get(id=pk)
    get_message = get_room.message_set.all()
    participants = get_room.participants.all()

    if request.method == 'POST':
        Message.objects.create(
            user=request.user,
            room=get_room,
            body=request.POST.get('body')
        )
        get_room.participants.add(request.user)
        return redirect('room', pk=get_room.id)

    context = {'room': get_room, 'messages': get_message, 'participants': participants}
    return render(request, 'base/room.html', context)


def userProfile(request, pk):
    user = User.objects.get(id=pk)
    rooms = user.room_set.all()
    room_messages = user.message_set.all()
    topic = Topic.objects.all()
    content = {'user': user, 'rooms': rooms, 'room_messages': room_messages, 'topics': topic}
    return render(request, 'base/profile.html', content)


@login_required(login_url='login')
def createRoom(request):
    form = RoomForm()
    topics = Topic.objects.all()
    if request.method == 'POST':
        topic_name = request.POST.get('topic')
        topic, created = Topic.objects.get_or_create(name=topic_name)

        Room.objects.create(
            host=request.user,
            topic=topic,
            name=request.POST.get('name'),
            description=request.POST.get('description')
        )

        return redirect('home')

    context = {"form": form, 'topics': topics}
    return render(request, 'base/create_room.html', context)


@login_required(login_url='login')
def updateRoom(request, pk):
    get_room = Room.objects.get(id=pk)
    form = RoomForm(instance=get_room)
    topics = Topic.objects.all()

    if request.user != get_room.host:
        return HttpResponse('You are not allowed here')  # zmienic ...

    if request.method == 'POST':
        topic_name = request.POST.get('topic')
        topic, created = Topic.objects.get_or_create(name=topic_name)
        get_room.name = request.POST.get('name')
        get_room.topic = topic
        get_room.description = request.POST.get('description')
        get_room.save()

        return redirect('home')

    context = {'form': form, 'topics': topics, 'room': get_room}
    return render(request, 'base/create_room.html', context)


@login_required(login_url='login')
def deleteRoom(request, pk):
    get_room = Room.objects.get(id=pk)

    if request.user != get_room.host:
        return HttpResponse('You are not allowed here')

    if request.method == 'POST':
        get_room.delete()
        return redirect('home')
    return render(request, 'base/delete.html', {'obj': get_room})


@login_required(login_url='login')
def deleteMessage(request, pk):
    message = Message.objects.get(id=pk)

    if request.user != message.user:
        return HttpResponse('You are not allowed here')

    if request.method == 'POST':
        message.delete()
        return redirect('home')

    return render(request, 'base/delete.html', {'obj': message})


@login_required(login_url='login')
def updateUser(request):
    user = request.user
    form = UserForm(instance=user)

    if request.method == 'POST':
        form = UserForm(request.POST, request.FILES, instance=user)
        if form.is_valid():
            form.save()
            return redirect('user_profile', pk=user.id)

    return render(request, 'base/update_user.html', {'form': form})


# PHONE:
def topicsPage(request):
    q = request.GET.get('q') if request.GET.get('q') is not None else ''
    topics = Topic.objects.filter(name__icontains=q)
    context = {'topics': topics}
    return render(request, 'base/topics.html', context)


def activityPage(request):
    room_messages = Message.objects.all()
    return render(request, 'base/activity.html', {'room_messages': room_messages})
